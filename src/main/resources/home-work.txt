contact-book

   person
      id     pk
      name
      surname

ID  name    surname
--------------------
1   Zaur    Mustafayev

----------------------------------------------------------------------
   contact
     id    pk
     contactType -> [EMAIL,PHONE,ADDRESS,SKYPE,LINKEDIN,FACEBOOK]
     value
     personId   fk


CRUD-
 id contactType   value                    personId
 ---------------------------------------------------
  1  PHONE        0556112116                1
  2  PHONE        0512297762                1
  3  EMAIL        zaurmustafayev@gmail.com  1
  4  SKYPE        zaur.mustafayev           1