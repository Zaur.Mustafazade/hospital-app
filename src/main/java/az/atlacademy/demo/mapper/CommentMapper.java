package az.atlacademy.demo.mapper;


import az.atlacademy.demo.model.Comment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class CommentMapper implements RowMapper<Comment> {

    @Override
    public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
        Comment s = new Comment();
        s.setId(rs.getLong("id"));
        s.setAuthor(rs.getString("author"));
        s.setComment(rs.getString("comment"));
        s.setEmail(rs.getString("email"));
        s.setIsPublished(rs.getInt("is_published"));
        s.setHospitalId(rs.getLong("hospital_id"));
        return s;
    }
}
