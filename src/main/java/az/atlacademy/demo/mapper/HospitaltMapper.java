package az.atlacademy.demo.mapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import az.atlacademy.demo.model.Hospital;

public class HospitaltMapper implements RowMapper<Hospital> {

    @Override
    public Hospital mapRow(ResultSet rs, int rowNum) throws SQLException {
        Hospital s = new Hospital();
        s.setId(rs.getLong("id"));
        s.setName(rs.getString("name"));
        s.setPhone(rs.getString("phone"));
        s.setEmail(rs.getString("email"));
        s.setAddress(rs.getString("address"));

        return s;
    }
}
