package az.atlacademy.demo.controller;


import az.atlacademy.demo.controller.response.BaseResponse;
import az.atlacademy.demo.exception.HospitalNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class MainControllerAdvice
        //  extends ResponseEntityExceptionHandler
{

    @ExceptionHandler(HospitalNotFoundException.class)
    public ResponseEntity<BaseResponse<String>> applicationException(HospitalNotFoundException e) {
        log.error(HospitalNotFoundException.class.toString());
        log.error(e.getMessage());
        BaseResponse<String> baseResponse = new BaseResponse<>();
        baseResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(baseResponse);
        //return new ResponseEntity<> (setMessage (e.getMessage ()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<BaseResponse<String>>  exceptions(Throwable e) {
        log.error(Throwable.class.toString());
        log.error(e.getMessage());
        e.printStackTrace();
        BaseResponse<String> baseResponse = new BaseResponse<>();
        baseResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(baseResponse);
        // return new ResponseEntity<> (setMessage (e.getMessage ()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<BaseResponse<String>> exceptions(Exception e) {
//        log.error(Exception.class.toString());
//        log.error(e.getMessage());
//        e.printStackTrace();
//
//        BaseResponse<String> baseResponse = new BaseResponse<>();
//        baseResponse.setMessage(e.getMessage());
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(baseResponse);
//
//
//        //return new ResponseEntity<> (setMessage (e.getMessage ()), HttpStatus.INTERNAL_SERVER_ERROR);
//    }


    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<BaseResponse<String>> exceptions(RuntimeException e) {
        log.error(RuntimeException.class.toString());
        log.error(e.getMessage());
        e.printStackTrace();
        BaseResponse<String> baseResponse = new BaseResponse<>();
        baseResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(baseResponse);


        //return new ResponseEntity<> (setMessage (e.getMessage ()), HttpStatus.INTERNAL_SERVER_ERROR);
    }




//    @Override
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
//                                                                  HttpHeaders headers,
//                                                                  HttpStatus status,
//                                                                  WebRequest request) {
//        BindingResult bindingResult = ex.getBindingResult ( );
//        if(bindingResult.getGlobalError () != null){
//            baseResponse.setError (bindingResult.getGlobalError ().getDefaultMessage ());
//        }else if(bindingResult.getFieldError () != null){
//            baseResponse.setError(bindingResult.getFieldError ().getDefaultMessage ());
//        }
//        return new ResponseEntity<> (
//                baseResponse,
//                HttpStatus.BAD_REQUEST
//        );
//    }
}
