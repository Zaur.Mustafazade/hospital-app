package az.atlacademy.demo.controller;


import az.atlacademy.demo.model.Hospital;
import az.atlacademy.demo.servise.HospitalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/hospitals")
@RestController
@RequiredArgsConstructor
@Slf4j
@Api(tags = "Hospital CRUD operations ")
@CrossOrigin
public class HospitalController {
    private final HospitalService hospitalService;


    @GetMapping
    public  ResponseEntity<List<Hospital>> getAll(){
        return ResponseEntity.ok(hospitalService.findAll());
    }

    @ApiOperation("Get information and comments about a hospital")
    @GetMapping("/{hospitalId}")
    public  ResponseEntity<Hospital> getById(
            @PathVariable("hospitalId")  Long hospitalId
    ){
        log.info("hospital id {} ",hospitalId);
        return ResponseEntity.ok(hospitalService.findById(hospitalId));
    }

    @ApiOperation("Create Hospital ")
    @PostMapping
    public  ResponseEntity<Hospital> create(
            @RequestBody  Hospital hospital
    ){
        log.info("hospital id {} ",hospital);

        if(hospitalService.create(hospital)){
          return   ResponseEntity.status(HttpStatus.CREATED).body(hospital) ;
        }else{
            return   ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null) ;
        }

    }
}
