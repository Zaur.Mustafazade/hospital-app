package az.atlacademy.demo.controller;


import az.atlacademy.demo.model.Comment;
import az.atlacademy.demo.servise.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/comments")
@RestController
@RequiredArgsConstructor
@Slf4j
@Api("Comment CRUD operations ")
public class CommentController {
    private final CommentService commentService;

    @ApiOperation(value = "Get all comments")
    @GetMapping
    public ResponseEntity<List<Comment>> getAll() {
        return ResponseEntity.ok(commentService.findAll());
    }

    @ApiOperation("Create comment ")
    @PostMapping
    public  ResponseEntity<Comment> create(
            @RequestBody Comment comment
    ){
        log.info("comment  {} ",comment);

        if(commentService.create(comment)){
            return   ResponseEntity.status(HttpStatus.CREATED).body(comment) ;
        }else{
            return   ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null) ;
        }

    }

    @ApiOperation("Create comment ")
    @PutMapping("/publish/{commentId}/{status}")
    public  ResponseEntity<Comment> publishStatus(
            @RequestParam("commentId")  Long commentId,
            @RequestParam("status")  Integer status
    ){
        log.info("comment  {} ",commentId);
        log.info("status  {} ",status);

        if(commentService.publish(commentId,status)){
            return   ResponseEntity.status(HttpStatus.CREATED).body(null) ;
        }else{
            return   ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null) ;
        }

    }

}
