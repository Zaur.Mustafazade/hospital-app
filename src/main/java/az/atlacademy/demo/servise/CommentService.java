package az.atlacademy.demo.servise;

import az.atlacademy.demo.model.Comment;
import az.atlacademy.demo.repository.CommentRepository;
import az.atlacademy.demo.repository.HospitalRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CommentService {

    private final CommentRepository commentRepository;
    private final HospitalRepository hospitalRepository;

    public List<Comment> findAll() {

        return commentRepository.getComments();
    }


    public boolean create(Comment comment) {
        log.info("Create comment  {}", comment);
        //  Hospital hospital=hospitalRepository.getHospitalById(comment.getHospitalId());
        boolean res = commentRepository.create(comment);
        log.info("result {}", res);
        return res;

    }

    public boolean publish(Long commentId, Integer publish) {
        log.info("Publish comment  {}", commentId);
        //  Hospital hospital=hospitalRepository.getHospitalById(comment.getHospitalId());
        boolean res = commentRepository.publish(commentId,publish);
        log.info("result {}", res);
        return res;

    }



}
