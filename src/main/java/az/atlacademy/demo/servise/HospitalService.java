package az.atlacademy.demo.servise;

import az.atlacademy.demo.model.Comment;
import az.atlacademy.demo.model.Hospital;
import az.atlacademy.demo.repository.CommentRepository;
import az.atlacademy.demo.repository.HospitalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@RequiredArgsConstructor
public class HospitalService {
    private final HospitalRepository hospitalRepository;
    private final CommentRepository commentRepository;

    private final Logger LOGGER = Logger.getLogger(HospitalService.class.getName());

    public List<Hospital> findAll() {
        LOGGER.info("Started.. find all");
        List<Hospital> hospitals = hospitalRepository.getAllHospitals();
        hospitals.forEach(h->{
            List<Comment> comments=commentRepository.getCommentsByHospital(h);
            h.setCommentCount(comments.size());
            h.setCommentList(comments);
        });
        LOGGER.info(String.format("all hopitals  - %s", hospitals));
        return hospitals;
    }

    public Hospital findById(Long id) {
        LOGGER.info("Started.. find by id = " + id);
        Hospital hospital = null;
        try {
            hospital = hospitalRepository.getHospitalById(id);

            hospital.setCommentList(commentRepository.getCommentsByHospital(hospital));
            LOGGER.info(String.format(" hopital info  - %s", hospital));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error {}", e.getMessage());
        }

        return hospital;
    }


    public boolean create(Hospital hospital) {
        LOGGER.info("Started.. create hospital = " + hospital);
        boolean status = false;
        try {
            status = hospitalRepository.create(hospital);


            LOGGER.info(String.format(" hopital created  - %s", hospital));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error {}", e.getMessage());
        }

        return status;
    }
}
