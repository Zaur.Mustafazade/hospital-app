package az.atlacademy.demo.repository;

public class SQLConstants {
    public static final String SQL_FIND_HOSPITAL= "select * from hospital where id = ?";
    public static final String SQL_DELETE_HOSPITAL = "delete from hospital where id = ?";
    public static final String SQL_UPDATE_HOSPITAL= "update hospital set name = ?, address = ?," +
            " email = ?, phone = ?  where id = ?";
    public static final String SQL_GET_ALL = "select * from hospital";
    public static final String SQL_INSERT_HOSPITAL = "insert into hospital(id, name, address,email,phone) " +
            "values(nextval('hospital_id'),?,?,?,?)";



    public static final String SQL_FIND_COMMENTS ="select * from comments ";
    public static final String SQL_FIND_COMMENT ="select * from comments where id = ?";
    public static final String SQL_FIND_COMMENTS_BY_HOSPITAL ="select * from comments where hospital_id = ? and is_published=1";
    public static final String SQL_INSERT_COMMENT ="insert into comments(id, author, comment,email,hospital_id,is_published) " +
            "values(nextval('comment_id'),?,?,?,?,0)";

    public static final String SQL_PUBLISH_COMMENT =" update  comments " +
            " set is_published=? " +
            " where id =? ";

}
