package az.atlacademy.demo.repository;

import az.atlacademy.demo.mapper.CommentMapper;
import az.atlacademy.demo.model.Comment;
import az.atlacademy.demo.model.Hospital;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
@RequiredArgsConstructor
public class CommentRepository {
    private final JdbcTemplate jdbcTemplate;


    public Comment getCommentById(Long id) {
        return (Comment) jdbcTemplate.queryForObject(SQLConstants.SQL_FIND_COMMENT, new Object[]{id}, new CommentMapper());
    }

    public List<Comment> getCommentsByHospital(Hospital hospital) {
        return jdbcTemplate.query(SQLConstants.SQL_FIND_COMMENTS_BY_HOSPITAL, new Object[]{hospital.getId()}, new CommentMapper());
    }


    public List<Comment> getComments() {
        return jdbcTemplate.query(SQLConstants.SQL_FIND_COMMENTS, new CommentMapper());
    }

    public boolean create(Comment comment) {

        return jdbcTemplate.update(SQLConstants.SQL_INSERT_COMMENT, new Object[]{

                comment.getAuthor(),
                comment.getComment(),
                comment.getEmail(),
                comment.getHospitalId()
        }) > 0 ? true : false;
    }

    public boolean publish(Long commentId, Integer publish) {

        return jdbcTemplate.update(SQLConstants.SQL_PUBLISH_COMMENT, new Object[]{publish,commentId
        }) > 0 ? true : false;
    }
}
