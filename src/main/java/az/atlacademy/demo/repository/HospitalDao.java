package az.atlacademy.demo.repository;

import az.atlacademy.demo.model.Hospital;

import java.util.List;

public interface HospitalDao {

    Hospital getHospitalById(Long id);

    List<Hospital> getAllHospitals();

    boolean delete(Hospital hospital);

    boolean update(Hospital hospital);

    boolean create(Hospital hospital);
}
