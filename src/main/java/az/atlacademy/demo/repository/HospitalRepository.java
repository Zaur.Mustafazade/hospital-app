package az.atlacademy.demo.repository;

import az.atlacademy.demo.mapper.HospitaltMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import az.atlacademy.demo.model.Hospital;
import az.atlacademy.demo.repository.SQLConstants;

import java.util.List;
//SQLConstants.SQL_GET_ALL, new HospitaltMapper()
@Repository
@RequiredArgsConstructor
public class HospitalRepository implements HospitalDao {
    private final JdbcTemplate jdbcTemplate;


    @Override
    public Hospital getHospitalById(Long id) {
        return jdbcTemplate.queryForObject(SQLConstants.SQL_FIND_HOSPITAL, new Object[]{id}, new HospitaltMapper());
    }

    @Override
    public List<Hospital> getAllHospitals() {
        return jdbcTemplate.query(SQLConstants.SQL_GET_ALL, new HospitaltMapper());
    }

    @Override
    public boolean delete(Hospital hospital) {
        return jdbcTemplate.update(SQLConstants.SQL_DELETE_HOSPITAL, new Object[]{hospital.getId()}) > 0 ? true : false;
    }

    @Override
    public boolean update(Hospital hospital) {
        return jdbcTemplate.update(SQLConstants.SQL_UPDATE_HOSPITAL, new Object[]{
                hospital.getName(),
                hospital.getAddress(),
                hospital.getEmail(),
                hospital.getPhone(),
                hospital.getId()}) > 0 ? true : false;
    }

    @Override
    public boolean create(Hospital hospital) {
        return jdbcTemplate.update(SQLConstants.SQL_INSERT_HOSPITAL, new Object[]{

                hospital.getName(),
                hospital.getAddress(),
                hospital.getEmail(),
                hospital.getPhone()
        }) > 0 ? true : false;
    }
}

