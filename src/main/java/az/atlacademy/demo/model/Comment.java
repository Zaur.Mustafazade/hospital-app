package az.atlacademy.demo.model;

import lombok.Data;

@Data
public class Comment {
    private Long id;
    private String author;
    private String comment;
    private String email;
    private Long  hospitalId;
    private Integer isPublished;
}
