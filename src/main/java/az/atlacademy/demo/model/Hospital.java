package az.atlacademy.demo.model;

import lombok.Data;

import java.util.List;

@Data
public class Hospital {
    private Long id;
    private String name;
    private String address;
    private String email;
    private String phone;
    private Integer commentCount;
    private List<Comment> commentList;
}
